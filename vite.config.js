import ViteComponents from "vite-plugin-components";
import { createVuePlugin } from "vite-plugin-vue2";
module.exports = {
    plugins: [createVuePlugin(), ViteComponents()],
};
