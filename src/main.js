import Vue from "vue";
import { GraphQLClient } from "graphql-request";
import Vuesax from "vuesax";
import "vuesax/dist/vuesax.css";
import router from "./router";
//import Toast from "vue-toastification";
import App from "./App.vue";

const graphcmsClient = new GraphQLClient(
  import.meta.env.VITE_GRAPHCMSKEY || process.env.GRAPHKEY
);

Vue.mixin({
  beforeCreate() {
    this.$graphcms = graphcmsClient;
  },
});

Vue.use(Vuesax);
new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
